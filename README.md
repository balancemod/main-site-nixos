## install
```bash
git clone https://gitlab.com/balancemod/main-site-nixos.git /tmp/nixconfig

# partition disk
sudo nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode disko /tmp/nixconfig/src/disko.nix --arg device /dev/sda

# install nixos
sudo nixos-install --root /mnt --flake gitlab:balancemod/main-site-nixos
```

## update
```
sudo nixos-rebuild switch --flake gitlab:balancemod/main-site-nixos
```