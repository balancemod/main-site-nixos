{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    
    bmod-beta = {
      url = "gitlab:balancemod/main-site/develop";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    bmod-draft = {
      url = "gitlab:balancemod/main-site/drafting";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    bmod-prod = {
      url = "gitlab:balancemod/main-site";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = { self,nixpkgs,... }@inputs:
  let 
  system = "x86_64-linux";
  specialArgs = {inherit self inputs;};
  lib = nixpkgs.lib;
  pkgs = import nixpkgs { inherit system; };
  in
  {
    # for auto update later
    flakeUrl = "gitlab:balancemod/main-site-nixos";
    nixosConfigurations.blutsauger = lib.nixosSystem {
      inherit specialArgs;
      modules = [
        ./src
      ];
    };

  };
}
