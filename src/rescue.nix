{ pkgs, ... }:
let
  netboot = import (pkgs.path + "/nixos/lib/eval-config.nix") {
      modules = [
        (pkgs.path + "/nixos/modules/installer/netboot/netboot-minimal.nix")
        module
      ];
    };
    module = {
      boot.supportedFilesystems = [ "btrfs" ];
    };
in {
  boot.loader.grub.extraEntries = ''
    menuentry "NixOS Installer" {
      linux ($drive1)/rescue-kernel init=${netboot.config.system.build.toplevel}/init ${toString netboot.config.boot.kernelParams}
      initrd ($drive1)/rescue-initrd
    }
  '';
  boot.loader.grub.extraFiles = {
    "rescue-kernel" = "${netboot.config.system.build.kernel}/bzImage";
    "rescue-initrd" = "${netboot.config.system.build.netbootRamdisk}/initrd";
  };
}