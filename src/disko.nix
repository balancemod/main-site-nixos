{
	device ? "/dev/sda",
	...
}:
{
	disko.devices = {
		disk = {
			main = {
				inherit device;
				content = {
					type = "gpt";
					partitions = {

						nix = {
							size = "100%";
							content = {
								type = "btrfs";
								extraArgs = [ "-f" ];
								subvolumes = {

									"/nix" = {
										mountpoint = "/nix";
										mountOptions = [ "subvol=nix" "compress=zstd" "noatime" ];
									};

									"/root" = {
										mountpoint = "/";
										mountOptions = [ "subvol=root" "compress=zstd" "noatime" ];
									};
									"/www" = {
										mountpoint = "/var/www";
										mountOptions = [ "subvol=www" "compress=zstd" "noatime" ];
									};
									"/home" = {
										mountpoint = "/home";
										mountOptions = [ "compress=zstd" ];
									};
									
								};
							};
						}; # </nix>
						
						boot = {
							size = "512M";
							type = "EF00";
							content = {
								type = "filesystem";
								format = "vfat";
								mountpoint = "/boot";
								mountOptions = [ "umask=007" ];
							};
						}; # </boot>
					}; # </partitions>
				};
			};# </main>
		};

		nodev = {
			"/tmp" = {
			fsType = "tmpfs";
		};
		}; # </nodev>
	};
}
