
# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs,self,inputs, ... }:
let 
createSite = 
{
stage,   # beta, drafts
name?"bmod-"+stage,
port?"3000",
package ? inputs.${name}.packages.${pkgs.system}.default,
cacheDir ? "/var/cache/bmod/${name}.json",
}:
  {
    enable = true;
    after = [ "network.target" ];
    wantedBy = [ "default.target" ];
    description = name;
    environment = {
      BMOD_CACHE = cacheDir;
      BMOD_SITE_STAGE=stage;
      NODE_ENV = "production";
      PORT = port;
    };
    serviceConfig = {
      WorkingDirectory = "${package}/lib/node_modules/bmod-site";
    };
    preStart = ''
      mkdir -p $(dirname ${cacheDir})
      touch ${cacheDir}
    '';
    script = "${package}/bin/bmod";
  };
in 
{

  

  systemd.services.bmod-beta = createSite {
    stage = "beta";
    port = "3080";
  };
  systemd.services.bmod-prod = createSite {
    stage = "prod";
    port = "3081";
  };
  systemd.services.bmod-draft = createSite {
    stage = "draft";
    port = "3082";
  };


  services.caddy = {
    enable = true;
    virtualHosts = {
      "beta.bmod.tf" = {
        serverAliases = [ "beta.balancemod.com" ];
        extraConfig = 
        let
          www = inputs.bmod-beta.lib.createStatic {
            BMOD_SITE_STAGE="beta";
            NODE_ENV = "production";
          };
          in
        ''
        
	        encode zstd gzip
          handle /api/* {
              @apiversion {
                  path_regexp version ^/api/v([2-9]|[1-9][0-9]+).*
              }
              reverse_proxy @apiversion localhost:3081
          }

          handle {
            root * ${www}
            file_server browse
          }
        '';
      };

      "bmod.tf" = {
        serverAliases = [ "balancemod.com" "www.bmod.tf" "www.balancemod.com" ];
        extraConfig = 
        let
          www = inputs.bmod-prod.lib.createStatic {
            BMOD_SITE_STAGE="prod";
            NODE_ENV = "production";
          };
          in
        ''
        
	        encode zstd gzip
          handle /api/* {
              @apiversion {
                  path_regexp version ^/api/v([2-9]|[1-9][0-9]+).*
              }
              reverse_proxy @apiversion localhost:3081
          }

          handle {
            root * ${www}
            file_server browse
          }
        '';
      };
      "draft.bmod.tf" = {
        serverAliases = [ "draft.balancemod.com" ];
        extraConfig = 
        let
          www = inputs.bmod-draft.lib.createStatic {
            BMOD_SITE_STAGE="draft";
            NODE_ENV = "production";
          };
          in
        ''
        
	        encode zstd gzip
          handle /api/* {
              @apiversion {
                  path_regexp version ^/api/v([2-9]|[1-9][0-9]+).*
              }
              reverse_proxy @apiversion localhost:3082
          }


          handle {
            root * ${www}
            file_server browse
          }
        '';
      };
    };
  };


   users = {
    mutableUsers = true;
    allowNoPasswordLogin = true;
    users = {
      tumble = {
        initialHashedPassword = "$6$eoTIFcyyOlyyxXGM$XJOpc721mGjlhjh6SOvVAttxMOnMFZGNmTzUTzq9itWcs0lsQECSCx8XeuJGUwoQa7W98he87xm2jtAd.QPE2.";
        #initialHashedPassword = "";
        description = "Tumble";
        isNormalUser = true;
        extraGroups = [ "wheel" ];
        # shell = pkgs.fish;

        openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIICjgzPnjrgY26R/kvx4o2fcwdVGfpis+vgNE5IcaiU6 tumble@latias"
        ];
      };

      zarlo = {
        initialHashedPassword = "$6$eoTIFcyyOlyyxXGM$XJOpc721mGjlhjh6SOvVAttxMOnMFZGNmTzUTzq9itWcs0lsQECSCx8XeuJGUwoQa7W98he87xm2jtAd.QPE2.";
        isNormalUser = true;
        description = "Doctor Zarlo";
        extraGroups = [ "wheel" ];

        openssh.authorizedKeys.keys = [
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDJ2unYas/ixEllIpHdu9bCXeBaHv5WhpY6hq+xrRU2AG18nBNEq5THqbAYYvqG66fy29s565/gnPzJDQy0WWQZPXvXjMoGD8HjN7ssMAoXm8rTijM+aq85duZovFhbPqjuyw/TEW8UzASh+u/bsMhKcsOCIE3Kx9xKwGdGGkfFvWF1PL3Nzss3jWidvqXM9zXKlOXUoOlQLFqwGjc+3jLO3fEBN+qBIJX7gOy5EqONBZpSu7q0mCL9JbJLmO1soCTCILjIxIqA3TeIALUexxqTDJZniqrIOQtLSiQWJjbwmZy7QlGe9DEcm6gtM0KcL3EhxPP1dVXgbWP4b2ka0ivkC7BpRPP+c4mY/7s9wHSrmOG5kmwqX1XtWtXyGpEqGEiFMs30im17WEYkvwJS5TC08tjeKbveE5I4VNz0dki1Cyx/oPehgN2kh6uqhpxfWmMHFjKTejYoQxDscxr6mj0DIJuUh2gMQGN9lrc8XdUL/6MKfeL9MO5j7mfDkeEDeiOyUHZ3k8ekDMbCNDSU6octJYN/y7ip2Kjp0yWrBhjLMWNBVbfURc/kd/dN4cI/HE1jQmHwf8OJuvqyUu7DSSoub7T5LUMJ92Kn9Oak+J+DiFePW+x/oOBtn+OONkOrt39eGt/EOjkX9oBn7RwZTXAguOPBhHsBtfsAId/P8EdFbw== 5899@zarlo.dev"
        ];
      };
    };
   };

  imports =
    [
    # Include the results of the hardware scan.
      ./hw.nix
      
      # include disko
      inputs.disko.nixosModules.default
      (import ./disko.nix { device = "/dev/disk/by-id/scsi-0QEMU_QEMU_HARDDISK_drive-scsi0"; })

      # hmm
      #"${home-manager}/nixos"

      # built in installer image
      ./rescue.nix
      
    ];

   system.autoUpgrade = {
    enable = true;
    flake = self.flakeUrl;
    dates = "daily";
    persistent = true;
    randomizedDelaySec = "45min";
    # kernel updates
    allowReboot = true;
    rebootWindow = {
      lower = "03:00";
      upper = "07:00";
    };
  };


  boot.loader.systemd-boot.enable = true;
        boot.loader.efi.canTouchEfiVariables = true;
  networking = {
    hostName = "blutsauger";


    usePredictableInterfaceNames = lib.mkDefault true;
    #useDHCP = false;

    interfaces.eth0 = {
      ipv4.addresses = [{
        address = "192.168.0.2";
        prefixLength = 24;
      }];
    };

    defaultGateway = {address = "192.168.0.1"; interface = "eth0";};
    nameservers = ["1.1.1.1"];


    firewall = {
      enable = true;
      allowedTCPPorts = [ 80 443 ];
    };
  };

  nix = {
    package = pkgs.lix;
    settings = {
      experimental-features = [ "nix-command" "flakes" ];
      auto-optimise-store = true;
    };
    # channel.enable = false;
    gc = {
      automatic = true;
      dates = "weekly";
    };
  };

  time.timeZone = "Europe/London";
  i18n.defaultLocale = "en_GB.UTF-8";


  environment.systemPackages = with pkgs; [
    sudo
    pass
    micro
    htop
    topgrade
    git
  ];

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    settings = {
      LogLevel = "VERBOSE"; 
       AllowGroups = ["users"];
                        PermitRootLogin = "no";
      MaxAuthTries = 99999;
     PasswordAuthentication = false;
                        KbdInteractiveAuthentication = false;
    };
  };

  # programs.fish.enable = true;




  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?

}
